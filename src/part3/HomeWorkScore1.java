package part3;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class HomeWorkScore1 {

	public void read() throws IOException {
		String line;
		Double score = 0.0;
		Double sum = 0.0;

		String filename = "exam.txt";
		FileReader fileReader = null;
		fileReader = new FileReader(filename);
		BufferedReader buffer = new BufferedReader(fileReader);

		System.out.print("  -------------HomeWork Score--------------\n");
		System.out.print("   Name, Average\n");
		System.out.println("  ==================");

		FileWriter fileWriter = null;
		fileWriter = new FileWriter("average.txt",true);
		PrintWriter out = new PrintWriter(new BufferedWriter(fileWriter));

		for (line = buffer.readLine(); line != null; line = buffer.readLine()) {
			String[] data = line.split(",");

			for (int i = 1; i < data.length; i++) {
				score = Double.parseDouble(data[i]);
				sum += score;
			}

			String name = data[0];
			out.println("   " + name + " " + sum / (data.length - 1));
			out.flush();
			System.out.println("   " + name + " " + sum / (data.length - 1));
			sum = 0.0;
		}

	}
}
