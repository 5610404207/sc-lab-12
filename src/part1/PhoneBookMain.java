package part1;
import java.io.IOException;

public class PhoneBookMain {
	public static void main(String[] args) throws IOException {
		PhoneBook phone = new PhoneBook();
		phone.read();
		phone.addNumber();
	}
}
